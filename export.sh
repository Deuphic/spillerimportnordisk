#!/usr/bin/env bash

cd result/
tar -cvf results.tar.gz ./*
scp results.tar.gz odin.minecraftfest.no:/home/minecraft/servers/ragnarok/nordisk/
cd ..
