#!/usr/bin/env python3

import sys
import requests

URL = "https://api.mojang.com/users/profiles/minecraft/{}"

# 0 - uuid
# 1 - name
# 2 - team_id
# 3 - language
TEAM = "INSERT INTO user (uuid, name, team_id, alive) VALUES ('{0}', '{1}', {2}, 1);"
WHITELIST = '{{"uuid": "{0}", "name": "{1}"}},'
LANGUAGE = "  {0}: {3}"

class Importer:

    def __init__(self, filename, team_id, language):
        self.filename = filename
        self.team_id = team_id
        self.language = language

        self.teams = []
        self.whitelist = []
        self.languages = []

    def reject(self, username):
        print(" ==> COULD NOT FIND: {}".format(username), file=sys.stderr)

    def handle_user(self, username, uuid):
        self.teams.append(TEAM.format(uuid, username, self.team_id, self.language))
        self.languages.append(LANGUAGE.format(uuid, username, self.team_id, self.language))
        self.whitelist.append(WHITELIST.format(uuid, username, self.team_id, self.language))

    def user(self, username):
        r = requests.get(URL.format(username))

        if r.status_code != 200:
            self.reject(username)
            return

        response = r.json()
        username = response['name']
        uuid = response['id']
        uuid = "{}-{}-{}-{}-{}".format(uuid[0:8], uuid[8:12], uuid[12:16], uuid[16:20], uuid[20:32])

        self.handle_user(username, uuid)

    def run(self):
        print(" :: Reading from: {}  Setting team {} and language {}".format(self.filename, self.team_id, self.language))

        with open(self.filename) as f:
            for line in f:
                name = line.rstrip('\n')
                self.user(name)

        print(" :: Writing results to whitelist.txt, teams.txt and languages.txt in ./result/")

        with open('./result/whitelist.json', 'a') as f:
            for line in self.whitelist:
                f.write("{}\n".format(line))
        with open('./result/teams.sql', 'a') as f:
            for line in self.teams:
                f.write("{}\n".format(line))
        with open('./result/languages.yml', 'a') as f:
            for line in self.languages:
                f.write("{}\n".format(line))

if __name__ == "__main__":
    Importer("./players/production_intl.txt", "NULL", "en").run()
    Importer("./players/production_no.txt", "NULL", "nb").run()
    Importer("./players/production_dr.txt", "NULL", "dk").run()
    Importer("./players/production_fi.txt", "NULL", "fi").run()

    Importer("./players/norway.txt", 1, "nb").run()
    Importer("./players/denmark.txt", 2, "dk").run()
    Importer("./players/finland.txt", 3, "fi").run()
